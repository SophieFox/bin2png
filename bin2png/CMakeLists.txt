cmake_minimum_required(VERSION 3.0)
project (bin2png)

add_executable(bin2png bin2png.c)
target_include_directories (bin2png PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../include)