#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#define STB_IMAGE_WRITE_STATIC
#undef STBI_WRITE_NO_STDIO
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <asprintf.h>

typedef unsigned long ulong_t;

int main(int argc, char** argv) {
  if (argc <= 1) {
    printf("Usage:\n %s <input file> [output png path]\n", argv[0]);
    exit(0);
  }

  HANDLE infile = CreateFile(argv[1], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (infile == INVALID_HANDLE_VALUE) {
    printf("Unable to open input file!\n");
    exit(1);
  }

  char* outPath;
  if (argc == 3) {
    outPath = argv[2];
  } else {
    asprintf(&outPath, "%s.png", argv[1]);
  }
  printf("Output path will be: %s\n", outPath);

  DWORD sz = GetFileSize(infile, NULL);
  printf("Input file is %u bytes long.\n", sz);

  ulong_t width, height;
  double root = sqrt((double)sz / 3.0f);
  width = height = floor(root);
  if((double)width != root) {
    printf("!! Warning: Resulting image not perfectly square, data truncated! (I'm working on it!)\n");
  }
  //height -= width % 2;
  printf("Resulting image dimensions: %lux%lux3\n", width, height);

  HANDLE map = CreateFileMapping(infile, NULL, PAGE_READONLY, 0, 0, NULL);
  if (map == NULL) {
    printf("Unable to CreateFileMapping!\n");
    exit(1);
  }

  LPCSTR data = (LPCSTR)MapViewOfFile(map, FILE_MAP_READ, 0, 0, 0);
  if (data == NULL) {
    printf("Unable to mmap file!\n");
    exit(1);
  }

  int ok = stbi_write_png(outPath, width, height, 3, data, (width*3));
  if (ok == 0) {
    printf("Failed to write png file! %d\n", ok);
    exit(1);
  }

  free(outPath);
  UnmapViewOfFile(data);
  CloseHandle(map);
  CloseHandle(infile);

  return 0;
}
