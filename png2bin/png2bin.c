#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#undef STBI_NO_STDIO
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <asprintf.h>

typedef unsigned long ulong_t;

int main(int argc, char** argv) {
  if (argc <= 1) {
    printf("Usage:\n %s <input file> [output binary path]\n", argv[0]);
    exit(0);
  }

  HANDLE infile = CreateFile(argv[1], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (infile == INVALID_HANDLE_VALUE) {
    printf("Unable to open input file!\n");
    exit(1);
  }

  char* outPath;
  if (argc == 3) {
    outPath = argv[2];
  } else {
    asprintf(&outPath, "%s.data", argv[1]);
  }
  printf("Output path will be: %s\n", outPath);

  DWORD sz = GetFileSize(infile, NULL);
  printf("Input file is %u bytes long.\n", sz);

  int width, height, channels;

  HANDLE map = CreateFileMapping(infile, NULL, PAGE_READONLY, 0, 0, NULL);
  if (map == NULL) {
    printf("Unable to CreateFileMapping!\n");
    exit(1);
  }

  LPCSTR data = (LPCSTR)MapViewOfFile(map, FILE_MAP_READ, 0, 0, 0);
  if (data == NULL) {
    printf("Unable to mmap file!\n");
    exit(1);
  }

  stbi_uc *buf = stbi_load_from_memory((stbi_uc*)data, sz, &width, &height, &channels, STBI_default);
  if (buf == 0 || buf == NULL) {
    printf("Failed to parse input png file! %d\n", buf);
  }
  printf("Found %dx%dx%d (%lu bytes) image data, writing to output path ...\n", width, height, channels, width*height*channels);


  FILE *out = fopen(outPath, "wb");
  fwrite((const void*)buf, width*height*channels, 1, out);
  fclose(out);


  free(outPath);
  UnmapViewOfFile(data);
  CloseHandle(map);
  CloseHandle(infile);

  return 0;
}
